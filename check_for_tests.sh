#!/bin/sh
set -e
echo "Checking all policies have test cases in directory: $1"
cd $1
set +e
for policy in *.sentinel
do
  test_dir=$(echo $policy | awk -F. '{print $1}')
  if [ ! -d "test/$test_dir" ]; then
    echo "No test cases for policy $policy in subfolder $test_dir"
    exit 1
  else
      cd test/$test_dir
      if ! ls fail*.json > /dev/null || ! ls pass*.json > /dev/null || ! ls mock*.sentinel > /dev/null; then
	  echo "Missing at least one of fail*.json, pass*.json or mock*.sentinel in for policy $policy in subfolder $test_dir";
	  exit 1
      fi
      cd ../../
  fi
done
