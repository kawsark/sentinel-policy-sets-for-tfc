policy "restrict-gce-machine-type" {
    enforcement_level = "soft-mandatory"
}

# Adding comment
policy "enforce-mandatory-labels" {
    enforcement_level = "hard-mandatory"
}
